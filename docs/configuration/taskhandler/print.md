# Print

Simple task that print to log file



Parameter                   | Required    | Type    |        Description                                           | Notes
----------------------------| ------------|---------|--------------------------------------------------------------|-------- 
label                       | yes         | String  | unique key                                                   | 
type                        | yes         | String  | simple (or other worker type task)                           | 
component                   | yes         | String  | Print type task                                              | must be Print 
text                        | yes         | String  | text to be printed                                           |

##Example

    {
        "label": "task3",
        "type": "simple",
        "component": "Print",        
        "text": "hello world ${var1}"
    }
