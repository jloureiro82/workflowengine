## WorkFlow Definition

example of a workflow definition, with one task, that print information

![hexagonal](../img/flow_simple_task_print.jpg)

    {
        "schemaVersion": 1,
        "version": 1,
        "restartable": false,
        "name": "transbankresult",
        "description": "transbankresult",
        "messengerProvider": "sync",      
        "defaultAction": "TERMINATE",        
        "pullContext": {
        	"stackId": "${workflow.token}"
        },
        "tasks": [
            {
                "label": "init",
                "name": "Print",
                "type": "simple",
                "description": "test",
                "text": "test"                
            }
        ],
        "outputParameters": {
            "init": "{context}",
            "code": "0"
        },
        "failureOutputParameters": {
            "init": "{context}",
            "code": "0"
        }
    }

Parameter        | Description                                                       | Notes
---------------- | ------------------------------------------------------------------|-------- 
name             | Name of the Workflow. Must be configured before starting workflow |
description      | Description of the workflow  |
tasks            | task to be executed  | see task section to see how to configure
pullContext      | get context from previous execution |
pushContext      | push context information to be saved |
inputParameters  | inputs expected by the workFlow | optional, the workflow engine accept any parameters, define only those that you need to enforce (mandatory)
outputParameters | returning parameters on the api in json format in case of failure in the execution this will be the default failure output | NOTE: only in sync mode using run api, in async configure webhooks to return parameters
webhooks         | Ability to register HTTP webhooks to receive notifications for certain events | ex: start/finished workflow/task
failureWorkflowId| Ability to call another workflow on the event of Rejection of the workflow configured| see more in [Failure](failure.md)
messengerProvider| Provider Used by the execution for the messaging | Example: "sync" 


