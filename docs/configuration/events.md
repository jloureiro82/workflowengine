## Events

Provides for loose coupling between workflows and support for producing and consuming events.

Producing and consuming events.
  
  * Action on specific events, some examples:
    * start a workflow when a specific event occurs that matches the provided criteria.
    * fail a task
    * complete a task    

