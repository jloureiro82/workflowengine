# WorkFlowEngine

Generic WorkFlow executor 

![flow](img/wf_overview.png)

##Main Features

* Build based on Hexagonal Architecture (Ports/Interfaces & Adapters pattern)
    *  Promotes decoupling from technology and frameworks.
* Execute tasks on a defined business flows (process) and provide full control 
* Visibility and traceability of all the steps of the process flows. 
* A JSON/YML DSL based definition the defines the execution flow
    * dynamic objects/templating 
* Synchronous execution mode
    * for deployments that require High Performance and real time response to upstream system.
* Asynchronous execution mode with error handling
    * Operational control over workflow with the ability to pause, resume, restart, retry and terminate    
* Generic execution API in json format
    * The input parameters and structure are not fixed.
    * Mapping system that can adapt to any input structure
    * Mapping available in all stages of execution, input, output and between tasks.    
* Standalone or server/client deployment 
    * Local workers or dedicate remote workers
    * Can work as a standalone or server/client with the server being the master and sending to several jobs to workers.
* Generic expression engine
    * [ExpressionEngine](https://gitlab.readinessit.com/pbu/java-commons/commons-expression) - (lib that extend Spring Expression Language)
    * Expose generic interface that allows other libs to be implementation
* Persistence independently and no dependency of any provider, supports:
    * No persistence (it will sent events to 3rd party, microservice, message bus)
    * Filesystem or GIT (only for configuration persistence)
    * In-memory datagrid for high performance
    * Major SQL providers like postgresql, myssql, oracle
    * NOSQL providers like mongodb are also supported
    * Expose repository interface that allow custom implementations if needed.
* Event handlers
    * Producing and consuming events.
    * Action on specific events, some examples:
        * Start a workflow when a specific event occurs that matches the provided criteria.
        * Fail a task
        * Complete a task    
* Messaging interface that supports:
    * Sync messaging (runs on main thread)
    * JMS
        * Apache Artemis native supported and recommended
        * Other options also available if they use jmstemplate (from spring boot)
    * Kafka
    * AMPQ
        * Rabbit
    * Other custom implementation if implements the messaging interface
* Caching
    * Simple cache implementation
    * Hazelcast for replicated cache
    * Expose interface that allow other implementations/providers if needed.
* Metrics
    * Prometheus
    * ElasticSearch
* Plugins and task handlers
    * Can dynamic register new task handlers, this can work as plugins.
    * One example is [RestEngine](https://gitlab.readinessit.com/pbu/java-commons/commons-restengine) (http lib that extend http to support templating )
* Cloud Native
    * Runs on cloud environment like kubernetes/docker



