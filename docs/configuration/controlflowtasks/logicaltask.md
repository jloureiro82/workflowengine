##### Logical tasks

A logical task can change the execution flow, it can force to terminate an execution if a condition is not meet.

![logical task](../../img/wf_logical.png)

## Task Definition Parameters

Parameter                   | Required    | Type    |        Description                                           | Notes
----------------------------| ------------|---------|--------------------------------------------------------------|-------- 
label                       | yes         | String  | unique key                                                   |
type                        | yes         | String  | logical type task                                          | must be logical
expression                  | yes         | String  | logical expression to be validate                            |
resultTrue                  | optional    | String  | action to execute when the expression is true, it can be terminate / continue |
resultFalse                 | optional    | String  | action to execute when the expression is true, it can be terminate / continue |
output                      | no          | Map     | return parameters, this can be used to give a error message to the api |

### Example

    {
        "label": "validateOperation",
        "name": "validateOperation",
        "type": "logical",
        "description": "validateOperation",
        "expression": "${execution.getAsset.output.code== '0'}",
        "resultTrue": "terminate",
        "output": {
            "code": "99"
            }
    }
