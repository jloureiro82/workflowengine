# MergeListTask

A Simple task that merge elements into a list

Parameter                   | Required    | Type    |        Description                                           | Notes
----------------------------| ------------|---------|--------------------------------------------------------------|-------- 
label                       | yes         | String  | unique key                                                   |
type                        | yes         | String  | simple (or other worker type task)                           |
component                   | yes         | String  | MergeListTask type task                                      | must be MergeListTask
listsToMerge                | yes         | List    | List of the objects to Merge                                 | see bellow
failureWorkflowId           | no          | String  | WorkflowId called on the failure of the task                 | see notes of the failure
failureInputParameters      | no          | Map     | Parameters added to the default context given on the failure | see notes of the failure

## Lists To Merge

the objects given inside the parameter 'listsToMerge' will be joined in a List that will be returned on the task, the
task will ignore the null elements.

## Output Structure

In the output section of the execution of the MergeListTask Task there will be the List that i's the result of the task
executed.

## Example

    {
        "label": "task3",
        "type": "simple",
        "component": "MergeListTask",        
        "listsToMerge": [
            "${workflow.taskEntry}",
            "${workflow.taskEntry1}"
        ]
    }
