## Task Execution

     "transbankadaptor": {
            "description": "test",
            "label": "transbankadaptor",
            "type": "simple",
            "priority": 0,
            "inputParameters": {
                "headers": {
                    "Content-Type": "application/json"
                },
                "body": {
                    "stores": [
                        {
                            "amount": "5000",
                            "commerceCode": "597044444402",
                            "buyOrder": "AAAAAAAAAAAA"
                        }
                    ],
                    "sessionId": "EEEEEEEEEEEE",
                    "returnUrl": "http://www.entel.cl/webpaytest/101/PROD/ok.iws",
                    "buyOrder": "AAAAAAAAAAAA",
                    "finalUrl": "http://www.entel.cl/webpaytest/101/PROD/nok.iws"
                },
                "url": "http://transbankadaptor-app:8283/api/web-pay-mall/init-transaction"
            },
            "output": {
                "code": "0",
                "actionUrl": "https://webpay3gint.transbank.cl/webpayserver/initTransaction",
                "token": "e33d974449cda97be33b324bde73b864eef07bd99aaec519e489f9981304d193"
            },
            "createTime": "2019-08-05T18:07:56.825083Z",
            "domain": "work",
            "name": "RestTemplate",
            "progress": 100,
            "action": null,
            "startTime": "2019-08-05T18:07:56.850191Z",
            "id": 1108,
            "endTime": "2019-08-05T18:07:58.591591Z",
            "taskId": "2abe71f1f7f1427a9c7de0dd4c5fe0a1",
            "workflowId": "f581a26fb1244cf1acdae9e189d868e9",
            "status": "COMPLETED"
        }
