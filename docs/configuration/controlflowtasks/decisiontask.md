## Decision tasks

Executes one and only one branch of execution based on the expression value.

![Decision tree](../../img/wf_decision.png)

## Task Definition Parameters

Parameter                   | Required    | Type    |        Description                                           | Notes
----------------------------| ------------|---------|--------------------------------------------------------------|-------- 
label                       | yes         | String  | unique key                                                   |
type                        | yes         | String  | terminate type task                                          | must be decision
expression                  | yes         | String  | logical expression to be validate                            |
cases                       | yes         | List    | expression to determine witch branch will be executed        | see below
default                     | yes         | List    | List of tasks to execute in case of no correspondence        |

#### Cases Definition Parameters

Parameter                   | Required    | Type    |        Description                                           | Notes
----------------------------| ------------|---------|--------------------------------------------------------------|-------- 
key                         | yes         | String  | unique key                                                   |
tasks                       | yes         | List    | List of tasks                                            | the same as task definition

### Example

    {
        "schemaVersion": 1,
        "name": "d1",
        "description": "test2",         
        "tasks": [
             {
                "label": "test3",
                "name": "teste3",
                "type": "decision",
                "description": "test",
                "expression": "${workflow.key}",
                "cases": [
                    {
                        "key": "hello",
                        "tasks": [
                            {
                                "type": "simple",
                                "name": "Print",
                                "label": "task1",
                                "text": "hello world 1"
                            },
                             {
                                "type": "simple",
                                "name": "Print",
                                "label": "task2",
                                "text": "hello world 2"
                            },
                            {
                                "type": "simple",
                                "name": "Print",
                                "label": "task3",
                                "text": "hello world 3"
                            }
                        ]
                    },
                    {
                        "key": "goodbye",
                        "tasks": [
                            {
                                "type": "simple",
                                "name": "Print",
                                "label": "taskg1",
                                "text": "goodbye world 1"
                            }
                        ]
                    }
                ]                
             },
              {
                    "type": "simple",
                    "name": "Print",
                    "label": "task4",
                    "text": "hello world 4"
                }
        ]        
    }
