## Failure Actions

Actions executed on the consideration that the execution it's a non-redeemable failure.

### Concept of Failure and Workflow States

The States considered for this concept are the state FAILED and REJECTED, for the workflow engine an execution that it's
on the state of FAILURE still permit some actions, to the user or other systems like retries, skipTasks and ultimately
rejection.

![failure](../img/failureWorkflow.png)

### TASK FAILURE

On the event of a failure in the execution of a task, there will be some possible outcomes:

* WORKFLOW -> FAILED
* WORKFLOW -> REJECTED

#### Failure Input Parameters

Only Available for the failure configured inside the Task.

In case of a failure configured inside a task there is possible to configure some customized input parameters for the
failure flow. those parameters will be processes before the task and be added to the input given to the failure
execution.

#### WORKFLOW -> FAILED

On this event the task and the workflow don't have any failure configuration, and the workflow will put the execution
as 'FAILED' in this state the workflow will permit some actions to the execution:

* SKIP_TASKS
* RETRY
* REJECT

#### WORKFLOW -> REJECTED

On this event the task of the workflow have failure configuration, that will lead the workflow to call the
failureWorkflowId configured, leaving the execution in a final state so that the workflow will not permit the execution
of any action on the execution.

### Failure Inputs

The inputs given to the failure execution will be determined by the fact if it's a failure triggered by the
configuration inside the task or the configuration contained in the workflow.

#### Task Triggered Failure

Parameters     | Type     | Description
---------------|----------|----------------------
taskContext    | Map      | All information sended to the task execution including the failure input Parameters
cause          | String   | Error cause (not always filled)
stackTrace     | List     | StackTrace with some detail of the error
type           | String   | Error Type
message        | String   | Error message
httpResponse   | Map      | Can be filled in case of the error being launched by and http task

Bellow we have and example of the entry of an execution started by a failure contained in a task.

    {
        "workflow": {
            "cause": null,
            "taskContext": {
                "instantFailure": true,
                "instantFailureCondition": "${body.code == 100}",
                "failureInputParameters": {
                    "accion": "BLOCK",
                    "data": {
                        "listMember": "56973766165",
                        "inputs": {
                            "area": "Bloqueo",
                            "orderType": "Orden",
                            "consumerId": "0014COLL_BLOQU202103041831370000000000005955010000",
                            "cleanDate": "2021-03-05T21:31:37.833+0000",
                            "externalId": "1.12927247",
                            "identificationType": "RUT",
                            "transactionId": "0014COLL_BLOQU202103041831370000000000005955010000",
                            "transactionReason": "S2",
                            "transactionDes": "Suspensión No pago-unidirecional",
                            "urlCallBack": "http://svc-msentitynotifications/services/msentitynotifications/entity/block/v1/update/11140484",
                            "processId": "00140000000134",
                            "orderCommercialChannel": "Legacy_RMCA",
                            "countryCode": "CHL",
                            "requestTimeStamp": "2021-03-04T21:31:37.817+0000",
                            "identificationNumber": "17413278-K",
                            "subArea": "Móvil - Postpago",
                            "id": 11140484,
                            "applicationCode": "SAP-RMCA",
                            "workflowId": "block",
                            "status": "CREATED"
                        }
                    }
                },
                "description": "getEmaActionPostPaid",
                "label": "getEmaActionPostPaid",
                "type": "simple",
                "priority": 0,
                "inputParameters": {
                    "headers": {
                        "Content-Type": "application/json"
                    },
                    "httpMethod": "POST",
                    "body": {
                        "addParameters": [
                            {
                                "@baseType": "string",
                                "@type": "string",
                                "valueType": "string",
                                "name": "hlrUserRsa",
                                "@schemaLocation": "string",
                                "value": "1"
                            },
                            {
                                "@baseType": "string",
                                "@type": "string",
                                "valueType": "string",
                                "name": "epsUserIndividualSubscribedChargingCharacteristic",
                                "@schemaLocation": "string",
                                "value": "1190"
                            },
                            {
                                "@baseType": "string",
                                "@type": "string",
                                "valueType": "string",
                                "name": "IMSI",
                                "@schemaLocation": "string",
                                "value": "730011025799810"
                            },
                            {
                                "@baseType": "string",
                                "@type": "string",
                                "valueType": "string",
                                "name": "MSISDN",
                                "@schemaLocation": "string",
                                "value": "56973766165"
                            }
                        ],
                        "transactionAction": "Suspend",
                        "currentSuspension": "SPOUT",
                        "service": "MobilePostpaid",
                        "targetSuspension": "S2"
                    },
                    "url": "http://svc-ordercareadaptor.collections30.svc.cluster.local/services/ordercareadaptor/v1/collections/actionMap"
                },
                "retryDelay": "3000",
                "rawRequest": false,
                "successCondition": "${body.code == 0}",
                "createTime": "2021-03-05T00:01:44.056139Z",
                "failureWorkflowId": "errorFlowGetEmaAction",
                "domain": "work",
                "name": "http",
                "startTime": "2021-03-05T00:01:53.101923Z",
                "taskNumber": 0,
                "id": null,
                "retry": "3",
                "retryAttempts": 3,
                "taskId": "3dafb4f7f0344736b16f26381fdc0bcd",
                "workflowId": "d8fdce15c1e54d42a551e0ce9640c648",
                "status": "FAILED"
            },
            "stackTrace": [
                "com.readinessit.workflowengine.core.error.SuccessConditionException: Success Condition ${body.code == 0} Failed",
                "\tat com.readinessit.workflowengine.core.taskHandler.Http.successConditionMethod(Http.java:183)",
                "\tat com.readinessit.workflowengine.core.taskHandler.Http.handle(Http.java:163)",
                "\tat com.readinessit.workflowengine.core.worker.SimpleWorker.handle(SimpleWorker.java:71)",
                "\tat com.readinessit.workflowengine.core.dispach.TaskDispatcherSimpleWorker.dispatch(TaskDispatcherSimpleWorker.java:17)",
                "\tat com.readinessit.workflowengine.core.dispach.TaskDispatcherSimpleWorker.dispatch(TaskDispatcherSimpleWorker.java:7)",
                "\tat com.readinessit.workflowengine.core.dispach.TaskDispatcherHandle.dispatch(TaskDispatcherHandle.java:25)",
                "\tat com.readinessit.workflowengine.core.error.TaskExecutionErrorHandler.handle(TaskExecutionErrorHandler.java:59)",
                "\tat com.readinessit.workflowengine.core.error.TaskExecutionErrorHandler.handle(TaskExecutionErrorHandler.java:26)",
                "\tat com.readinessit.workflowengine.core.error.ErrorHandlerResolver.handle(ErrorHandlerResolver.java:23)",
                "\tat com.readinessit.workflowengine.core.WorkflowService.handleError(WorkflowService.java:427)",
                "\tat com.readinessit.workflowengine.core.WorkflowService$$FastClassBySpringCGLIB$$67f2bbce.invoke(<generated>)",
                "\tat org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:218)",
                "\tat org.springframework.aop.framework.CglibAopProxy$DynamicAdvisedInterceptor.intercept(CglibAopProxy.java:684)",
                "\tat com.readinessit.workflowengine.core.WorkflowService$$EnhancerBySpringCGLIB$$277cb9db.handleError(<generated>)",
                "\tat jdk.internal.reflect.GeneratedMethodAccessor522.invoke(Unknown Source)",
                "\tat java.base/jdk.internal.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)",
                "\tat java.base/java.lang.reflect.Method.invoke(Unknown Source)",
                "\tat org.springframework.util.MethodInvoker.invoke(MethodInvoker.java:280)",
                "\tat org.springframework.jms.listener.adapter.MessageListenerAdapter.invokeListenerMethod(MessageListenerAdapter.java:294)",
                "\tat org.springframework.jms.listener.adapter.MessageListenerAdapter.onMessage(MessageListenerAdapter.java:222)",
                "\tat org.springframework.jms.listener.adapter.AbstractAdaptableMessageListener.onMessage(AbstractAdaptableMessageListener.java:216)",
                "\tat brave.jms.TracingMessageListener.onMessage(TracingMessageListener.java:49)",
                "\tat org.springframework.jms.listener.AbstractMessageListenerContainer.doInvokeListener(AbstractMessageListenerContainer.java:761)",
                "\tat org.springframework.jms.listener.AbstractMessageListenerContainer.invokeListener(AbstractMessageListenerContainer.java:699)",
                "\tat org.springframework.jms.listener.AbstractMessageListenerContainer.doExecuteListener(AbstractMessageListenerContainer.java:674)",
                "\tat org.springframework.jms.listener.AbstractPollingMessageListenerContainer.doReceiveAndExecute(AbstractPollingMessageListenerContainer.java:318)",
                "\tat org.springframework.jms.listener.AbstractPollingMessageListenerContainer.receiveAndExecute(AbstractPollingMessageListenerContainer.java:257)",
                "\tat org.springframework.jms.listener.DefaultMessageListenerContainer$AsyncMessageListenerInvoker.invokeListener(DefaultMessageListenerContainer.java:1189)",
                "\tat org.springframework.jms.listener.DefaultMessageListenerContainer$AsyncMessageListenerInvoker.executeOngoingLoop(DefaultMessageListenerContainer.java:1179)",
                "\tat org.springframework.jms.listener.DefaultMessageListenerContainer$AsyncMessageListenerInvoker.run(DefaultMessageListenerContainer.java:1076)",
                "\tat java.base/java.lang.Thread.run(Unknown Source)"
            ],
            "type": "successCondition",
            "message": "Success Condition ${body.code == 0} Failed",
            "httpResponse": {
                "headers": {
                    "Pragma": [
                        "no-cache"
                    ],
                    "Referrer-Policy": [
                        "strict-origin-when-cross-origin"
                    ],
                    "X-Frame-Options": [
                        "DENY"
                    ],
                    "Content-Type": [
                    "application/json;charset=UTF-8"
                    ],
                    "Cache-Control": [
                        "no-cache, no-store, max-age=0, must-revalidate"
                    ]
                },
                "body": {
                    "code": 100,
                    "message": "Action not Found"
                },
                "statusCode": 200
            }
        }
    }

#### Workflow Triggered Failure

For the Failure triggered by the Workflow contained configuration, since the hypothesis it's that can be called at any
moment of the execution of the execution, then the result for the inputs it's that will be a plenty more reduced. This
feature it's recommended for having generic error treatments that only needs the information contained on the call of
the execution "Inputs" of the execution. the inputs given to this kind of failure will be the same as the ones contained
in the original execution.

##### Configuration Examples

In the Following Example the task don't have any configuration of failure making so that the failure will be called with
the configuration contained on the workflow.

    {
        "schemaVersion": 1,
        "restartable": false,
        "pullContext": {
            "stackId": "${inputs.workflow.transactionId}"
        },
        "description": "failedWorkflowTest",
        "workflowStatusListenerEnabled": false,
        "name": "failedWorkflowTest",
        "failureWorkflowId": "failure",
        "workflowId": "failedWorkflowTest",
        "tasks": [
            {
                "label": "registrarTransaccionDeCanales",
                "name": "RestTemplate",
                "type": "simple",
                "description": "Registra Transacción recibida desde Canales",
                "inputParameters": {
                    "url": "http://logging-app:7286/services/logging/v1/generic/queue/Subscriptions",
                    "headers": {
                        "Content-Type": "application/json"
                    },
                    "body": {
                        "channelId": "${workflow.inputs.channel.id}",
                        "consumerId": "${workflow.headers.containsKey('consumerId')? workflow.headers.consumerId: workflow.soapHeaders.consumer.sysCode}",
                    }
                },
                "domain": "work"
            }
        ],
        "outputParameters": {
            "jsonPath": "${execution}"
        }
    }

In the Following Example the task contains the configuration for the failure leading to be called by the configuration
contained inside the task.

    {
        "schemaVersion": 1,
        "restartable": false,
        "pullContext": {
            "stackId": "${inputs.workflow.transactionId}"
        },
        "description": "failedWorkflowTest",
        "workflowStatusListenerEnabled": false,
        "name": "failedWorkflowTest",
        "workflowId": "failedWorkflowTest",
        "tasks": [
            {
                "label": "registrarTransaccionDeCanales",
                "name": "RestTemplate",
                "type": "simple",
                "failureWorkflowId": "failureTask",
                "failureWorkflowInputParameters": {
                    "entry": "${workflow.input}"
                }
                "description": "Registra Transacción recibida desde Canales",
                "inputParameters": {
                    "url": "http://logging-app:7286/services/logging/v1/generic/queue/Subscriptions",
                    "headers": {
                        "Content-Type": "application/json"
                    },
                    "body": {
                        "channelId": "${workflow.inputs.channel.id}",
                        "consumerId": "${workflow.headers.containsKey('consumerId')? workflow.headers.consumerId: workflow.soapHeaders.consumer.sysCode}",
                    }
                },
                "domain": "work"
            }
        ],
        "outputParameters": {
            "jsonPath": "${execution}"
        }
    }
