# Webhooks

HTTP notifications for certain events.
Http request task if the execution flow to notify external systems

## Webhooks Definition Parameters

Parameter        | Description                                                       | Notes
---------------- | ------------------------------------------------------------------|-------- 
type             | Event Type        | workflow.status
status           | workflow status   | optional, if status not specified, applies to all status 
url              | endpoint to sent request  | 
headers          | http headers      | optional (default application/json default)
body             | request to send (expression are supported by expression engine   | map format will send json, string format supported, change the headers to the type


## Example

    {
     (...),   
      "webhooks": [{
        "type": "workflow.status", 
        "status": "COMPLETED"
        "url": "http://example.com"
        "headers": {
          "Content-Type": "application/json"
        },
        "body": {
            "param1": "${workflow.inputs.param1}",
            "param2": "${execution.taskLabel.param2}",
            "param3": "${event.status}",
            .....
        }
        ....
      }]
    }