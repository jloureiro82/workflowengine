# JsonPath

Simple task analyse the JsonPath of an entry.

Parameter                   | Required    | Type    |        Description                                           | Notes
----------------------------| ------------|---------|--------------------------------------------------------------|-------- 
label                       | yes         | String  | unique key                                                   |
type                        | yes         | String  | simple (or other worker type task)                           |
component                   | yes         | String  | JsonPath type task                                           | must be jsonPath
entry                       | yes         | String or Map | entry to be analysed by the jsonPath                   |
schema                      | yes         | Map<String, String>  | map that contains the json paths to be processed|
failureWorkflowId           | no          | String  | WorkflowId called on the failure of the task                 | see notes of the failure
failureInputParameters      | no          | Map     | Parameters added to the default context given on the failure | see notes of the failure

## Complex Entry

## Errors

On the occurrence of a error of invalid JsonPath the output will fill that key with the value 'null'. This must be taken
in consideration when analysing the execution and configuring the Workflow Definition.

## Output Structure

In the output section of the execution of the JsonPath Task there will be the structure of the entry where the values
will be filled with the results of the task.

## Example

    {
        "label": "task3",
        "type": "simple",
        "component": "jsonPath",        
        "entry": "${workflow.taskEntry}"
        "schema": {
            "schemaEntry" "$.test.test"
        }
    }
