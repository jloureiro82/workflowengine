## Workflow Execution

        {
            "outputs": {
                "code": "0",
                "actionUrl": "https://webpay3gint.transbank.cl/webpayserver/initTransaction",
                "token": "e33d974449cda97be33b324bde73b864eef07bd99aaec519e489f9981304d193"
            },
            "execution": {
                "transbankadaptor": {
                    "description": "test",
                    "label": "transbankadaptor",
                    "type": "simple",
                    "priority": 0,
                    "inputParameters": {
                        "headers": {
                            "Content-Type": "application/json"
                        },
                        "body": {
                            "stores": [
                                {
                                    "amount": "5000",
                                    "commerceCode": "597044444402",
                                    "buyOrder": "AAAAAAAAAAAA"
                                }
                            ],
                            "sessionId": "EEEEEEEEEEEE",
                            "returnUrl": "http://www.entel.cl/webpaytest/101/PROD/ok.iws",
                            "buyOrder": "AAAAAAAAAAAA",
                            "finalUrl": "http://www.entel.cl/webpaytest/101/PROD/nok.iws"
                        },
                        "url": "http://transbankadaptor-app:8283/api/web-pay-mall/init-transaction"
                    },
                    "output": {
                        "code": "0",
                        "actionUrl": "https://webpay3gint.transbank.cl/webpayserver/initTransaction",
                        "token": "e33d974449cda97be33b324bde73b864eef07bd99aaec519e489f9981304d193"
                    },
                     "createTime": "2019-08-05T18:07:56.825083Z",
                    "domain": "work",
                    "name": "RestTemplate",
                    "progress": 100,
                    "action": null,
                    "startTime": "2019-08-05T18:07:56.850191Z",
                    "id": 1108,
                    "endTime": "2019-08-05T18:07:58.591591Z",
                    "taskId": "2abe71f1f7f1427a9c7de0dd4c5fe0a1",
                    "workflowId": "f581a26fb1244cf1acdae9e189d868e9",
                    "status": "COMPLETED"
                }
            },
            "workflowDefinition": {
                "schemaVersion": 1,
                "restartable": false,
                "executionType": "sync",
                "description": "webpayinit",
                "version": 1,
                "workflowStatusListenerEnabled": false,
                "defaultAction": "TERMINATE",
                "pushContext": {
                    "stackId": "${execution.transbankadaptor.output.token}",
                    "ttl": "84600",
                    "stackParameters": {
                        "inputs": "${inputs}",
                        "token": "${execution.transbankadaptor.output.code}"
                    }
                },
                "name": "webpayinit",
                "id": 1001,
                "failureWorkflow": null,
                "workflowId": "test",
                "tasks": [
                    {
                        "label": "transbankadaptor",
                        "name": "RestTemplate",
                        "type": "simple",
                        "description": "test",
                        "inputParameters": {
                            "url": "http://transbankadaptor-app:8283/api/web-pay-mall/init-transaction",
                            "headers": {
                                "Content-Type": "application/json"
                            },
                            "body": {
                                "buyOrder": "${workflow.buyOrder}",
                                "sessionId": "${workflow.sessionId}",
                                "returnUrl": "${workflow.returnUrl}",
                                "finalUrl": "${workflow.finalUrl}",
                                "stores": [
                                    {
                                        "amount": "${workflow.amount}",
                                        "commerceCode": "${workflow.commerce}",
                                        "buyOrder": "${workflow.buyOrder}"
                                    }
                                ]
                            }
                        },
                        "domain": "work"
                    }
                ],
                "outputParameters": {
                    "actionUrl": "${execution.transbankadaptor.output.actionUrl}",
                    "token": "${execution.transbankadaptor.output.token}",
                    "code": "${execution.transbankadaptor.output.code}"
                }
            },
            "inputs": {
                "workflow": {
                    "amount": "5000",
                    "contextPath": "https://portalcautivo.entel.cl/",
                    "channel": "WEB",
                    "sessionId": "EEEEEEEEEEEE",
                    "commerce": "597044444402",
                    "env": "PROD",
                    "market": "PP",
                    "provider": "TRANSBANK",
                    "msisdn": "56912345678",
                    "portal": "RECHARGE",
                    "returnUrl": "http://www.entel.cl/webpaytest/101/PROD/ok.iws",
                    "buyOrder": "AAAAAAAAAAAA",
                    "finalUrl": "http://www.entel.cl/webpaytest/101/PROD/nok.iws",
                    "opOcc": "",
                    "opUrl": "",
                    "workflowId": "webpayinit"
                }
            },
            "currentTask": -1,
            "label": null,
            "priority": 0,
            "tags": [
                ""
            ],
            "webhooks": null,
            "createTime": "2019-08-05T18:07:56.825083Z",
            "startTime": "2019-08-05T18:07:56.850191Z",
            "id": 1058,
            "endTime": "2019-08-05T18:07:58.591591Z",
            "workflowId": "f581a26fb1244cf1acdae9e189d868e9",
            "status": "COMPLETED"
        }
